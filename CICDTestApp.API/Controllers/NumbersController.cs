﻿using Microsoft.AspNetCore.Mvc;

namespace CICDTestApp.API.Controllers
{
    [Route("api/[controller]")]
    public class NumbersController : Controller
    {
        private readonly INumberService numService;

        public NumbersController()
        {
            numService = new NumberService();
        }

        // Get api/numbers/operation
        [HttpGet("{operation}")]
        public int DoOperation(string operation, int numA, int numB)
        {
            numService.SetNumbers(numA, numB);
            int result = 0;

            switch (operation)
            {
                case "add":
                    result = numService.Add();
                    break;

                case "multiply":
                    result = numService.Multiply();
                    break;
            }

            return result;
        }
    }
}
