using NUnit.Framework;
using CICDTestApp.API.Controllers;

namespace CICDTestApp.Tests
{
    /// <summary>
    ///     Tests for the Number Controller
    /// </summary>
    [TestFixture]
    public class NumberControllerTests
    {
        /// <summary>
        ///     The Number Controller
        /// </summary>
        private NumbersController testController;

        [SetUp]
        public void Setup()
        {
            testController = new NumbersController();
        }

        [TestCase(1, 3, 4)]
        [TestCase(4, 3, 7)]
        [TestCase(5, 6, 11)]
        [TestCase(7, 5, 12)]
        [TestCase(12, 3, 15)]
        public void CanAddNumbers(int numA, int numB, int expectedResult)
        {
            int actualResult = testController.DoOperation("add", numA, numB);
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestCase(1, 3, 3)]
        [TestCase(4, 3, 12)]
        [TestCase(5, 6, 30)]
        [TestCase(7, 5, 35)]
        [TestCase(12, 3, 36)]
        public void CanMultiplyNumbers(int numA, int numB, int expectedResult)
        {
            int actualResult = testController.DoOperation("multiply", numA, numB);
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}