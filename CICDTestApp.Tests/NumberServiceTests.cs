using NUnit.Framework;

namespace CICDTestApp.Tests
{
    /// <summary>
    ///     Tests for the Number Service
    /// </summary>
    [TestFixture]
    public class NumberServiceTests
    {
        /// <summary>
        ///     The Number Service
        /// </summary>
        private INumberService Service;

        [SetUp]
        public void Setup()
        {
            Service = new NumberService();
        }

        [Test]
        public void SetNumbersToUseAndVerifyValues()
        {
            int expectedNumberOne = 66;
            int expectedNumberTwo = 77;
            
            Service.SetNumbers(expectedNumberOne, expectedNumberTwo);
            int actualNumberOne = Service.GetFirstNumber();
            int actualNumberTwo = Service.GetSecondNumber();

            Assert.AreEqual(expectedNumberOne, actualNumberOne, "First number assigned correctly");
            Assert.AreEqual(expectedNumberTwo, actualNumberTwo, "Second number assigned correctly");
        }

        [Test]
        public void AddTheNumbers()
        {
            int numberOne = 2;
            int numberTwo = 4;

            Service.SetNumbers(numberOne, numberTwo);
            int actualresult = Service.Add();
            int expectedResult = numberOne + numberTwo;

            Assert.AreEqual(expectedResult, actualresult, "First and Second numbers are added together");
        }

        [Test]
        public void MultiplyTheNumbers()
        {
            int numberOne = 2;
            int numberTwo = 4;

            Service.SetNumbers(numberOne, numberTwo);
            int actualresult = Service.Multiply();
            int expectedResult = numberOne * numberTwo;

            Assert.AreEqual(expectedResult, actualresult, "First and Second numbers are multiplied together");
        }

        [Test]
        public void DoubleAWillDoubleTheFirstNumber()
        {
            int numberOne = 2;
            int numberTwo = 4;

            Service.SetNumbers(numberOne, numberTwo);
            Service.DoubleA();

            int expectedResult = numberOne * 2;

            Assert.AreEqual(expectedResult, Service.GetFirstNumber(), "First number is doubled");
        }

        [Test]
        public void DoubleBWillDoubleTheSecondNumber()
        {
            int numberOne = 2;
            int numberTwo = 4;

            Service.SetNumbers(numberOne, numberTwo);
            Service.DoubleB();

            int expectedResult = numberTwo * 2;

            Assert.AreEqual(expectedResult, Service.GetSecondNumber(), "Second number is doubled");
        }
    }
}