﻿namespace CICDTestApp
{
    /// <summary>
    ///     Interface for the Number Service
    /// </summary>
    public interface INumberService
    {
        /// <summary>
        ///     Set the numbers the service will use
        /// </summary>
        /// <param name="numA">The First Number</param>
        /// <param name="numB">The Second Number</param>
        void SetNumbers(int numA, int numB);

        /// <summary>
        ///     Get The First Number
        /// </summary>
        int GetFirstNumber();

        /// <summary>
        ///     Get The Second Number
        /// </summary>
        int GetSecondNumber();

        /// <summary>
        ///     Multiply the numbers
        /// </summary>
        int Multiply();

        /// <summary>
        ///     Add the numbers
        /// </summary>
        int Add();

        /// <summary>
        ///     Double A
        /// </summary>
        void DoubleA();

        /// <summary>
        ///     Double B
        /// </summary>
        void DoubleB();
    }
}