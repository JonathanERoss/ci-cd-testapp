﻿namespace CICDTestApp
{
    /// <summary>
    ///     Service to provide methods for working with numbers
    /// </summary>
    public class NumberService : INumberService
    {
        /// <summary>
        ///     The First Number
        /// </summary>
        private int FirstNumber { get; set; }

        /// <summary>
        ///     The Second Number
        /// </summary>
        private int SecondNumber { get; set;}

        /// <summary>
        ///     Set the numbers the service will use
        /// </summary>
        /// <param name="numA">The First Number</param>
        /// <param name="numB">The Second Number</param>
        public void SetNumbers(int numA, int numB)
        {
            FirstNumber = numA;
            SecondNumber = numB;
        }

        /// <summary>
        ///     Get The First Number
        /// </summary>
        public int GetFirstNumber() => FirstNumber;

        /// <summary>
        ///     Get The Second Number
        /// </summary>
        public int GetSecondNumber() => SecondNumber;

        /// <summary>
        ///     Multiply the numbers
        /// </summary>
        public int Multiply() => FirstNumber * SecondNumber;

        /// <summary>
        ///     Add the numbers
        /// </summary>
        public int Add() =>  FirstNumber + SecondNumber;

        /// <summary>
        ///     Double A
        /// </summary>
        public void DoubleA() => FirstNumber = FirstNumber * 2;

        /// <summary>
        ///     Double B
        /// </summary>
        public void DoubleB() => SecondNumber = SecondNumber * 2;

    }
}
