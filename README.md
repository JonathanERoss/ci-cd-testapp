# CI-CD-TestApp
Application for Testing CI/CD

## Overview
This application is a .NET Core 2.0 class library with WebAPI written in C#.  There are three projects. One for the class library, another for the API, and a final project for unit tests.

- CICDTestApp
- CICDTestApp.API
- CICDTestApp.Tests

The purpose of this application is to provide something small to test CI/CD pipelines.

## Running The Tests

From the command line, run the following in the root of the repository:

    dotnet test .\CICDTestApp.Tests\CICDTestApp.Tests.csproj --verbosity normal

This will build the solution, then execute the tests.  You may also build using the following commands.

	dotnet clean
	dotnet build

The build step will fetch all nuget packages required of the projects.  It is not necessary, but to manually restore the packages simply execute the following command:

    dotnet restore

You may also open the solution file (CICDTestApp.sln) with Visual Studio and run tests.

## Author
- Jonathan E. Ross
